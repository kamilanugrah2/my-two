// B. Tugas Conditional
``
var nama = "Jenita"
var peran = "Guard"

if (nama == '' && peran == '') {
    console.log("Nama harus diisi!")
} else if (nama == 'Jon' && peran == '') {
    console.log("Halo Jon, Pilih peranmu untuk memulai game")
} else if (nama == 'Jan' && peran == 'Penyihir') {
    console.log("Selamat datang di Dunia Werewolf, Jan")
    console.log("Halo Penyihir Jan, kamu dapat melihat siapa yang menjadi werewolf")
} else if (nama == 'Jenita' && peran == 'Guard') {
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'junaedi' && peran == 'Werewolf') {
    console.log("Selamat datang di Dunia Werewolf, junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam.")
}

// Switch Case
var tanggal = 21;
var bulan = 1;
var tahun = 1945;

switch (bulan) {
    case 1: {
        console.log(tanggal + ' Januari ' + tahun);
        break;
    }
    case 2: {
        console.log(tanggal + ' Februari ' + tahun);
        break;
    }
    case 3: {
        console.log(tanggal + ' Maret ' + tahun);
        break;
    }
    case 4: {
        console.log(tanggal + ' April ' + tahun);
        break;
    }
    case 5: {
        console.log(tanggal + ' Mei ' + tahun);
        break;
    }
    case 6: {
        console.log(tanggal + ' Juni ' + tahun);
        break;
    }
    case 7: {
        console.log(tanggal + ' Juli ' + tahun);
        break;
    }
    case 8: {
        console.log(tanggal + ' Agustus ' + tahun);
        break;
    }
    case 9: {
        console.log(tanggal + ' September ' + tahun);
        break;
    }
    case 10: {
        console.log(tanggal + ' OKtober ' + tahun);
        break;
    }
    case 11: {
        console.log(tanggal + ' November ' + tahun);
        break;
    }
    case 12: {
        console.log(tanggal + ' Deesember ' + tahun);
        break;
    }
    default: {
        console.log('Tidak terjadi apa-apa');
    }
}